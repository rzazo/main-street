export const SEPARATOR = {
    DOT: '.',
    SLASH: '/',
    COMA: ',',
};

export const MAGIC_NUMBER = {
    ZERO: 0,
    ONE: 1,
    LESS_ONE: -1,
};

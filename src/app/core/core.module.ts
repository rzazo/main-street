import { NgModule } from '@angular/core';

import { I18nModule } from './i18n';
import { PipeModule } from './pipes';
import { LoaderModule } from './loader';
import { DirectiveModule } from './directives/';
import { HttpClientInterceptorModule } from './http-client-interceptor';

@NgModule({
    imports: [
        PipeModule,
        LoaderModule,
        DirectiveModule,
        I18nModule.forRoot(),
        HttpClientInterceptorModule,
    ],
})

export class CoreModule {}
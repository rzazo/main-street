export const KEY_CODE = {
    DELETE: 46,
    BACK_SPACE: 8,
    TAB: 9,
    ESC: 27,
    ENTER: 13,
    END: 35,
    RIGHT: 39,
    A: 65,
    C: 67,
    V: 86,
    X: 88,
    ZERO: 48,
    NINE: 57,
    NUM_PAD_ZERO: 96,
    NUM_PAD_NINE: 105,
};

export const ACTION_KEYS = [
    KEY_CODE.DELETE,
    KEY_CODE.BACK_SPACE,
    KEY_CODE.TAB,
    KEY_CODE.ESC,
    KEY_CODE.ENTER,
];

export const CTRL_COMBINATION = [
    KEY_CODE.A,
    KEY_CODE.C,
    KEY_CODE.V,
    KEY_CODE.X,
];

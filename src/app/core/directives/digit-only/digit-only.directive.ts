import { Directive, ElementRef, HostListener } from '@angular/core';

import { ACTION_KEYS, CTRL_COMBINATION, KEY_CODE } from './constants';

@Directive({
    selector: '[digitOnly]'
})
export class DigitOnlyDirective {
    public inputElement: HTMLElement;

    constructor(public el: ElementRef) {
        this.inputElement = el.nativeElement;
    }

    @HostListener('keydown', ['$event'])
    public onKeyDown(e: KeyboardEvent) {
        if (
            ACTION_KEYS.indexOf(e.keyCode) !== -1 ||
            ( CTRL_COMBINATION.includes(e.keyCode) && e.ctrlKey === true ) ||
            ( CTRL_COMBINATION.includes(e.keyCode) && e.metaKey === true ) ||
            ( e.keyCode >= KEY_CODE.END && e.keyCode <= KEY_CODE.RIGHT )
        ) {
            return;
        }
        this.preventDefault(e);
    }

    @HostListener('paste', ['$event'])
    public onPaste(event: ClipboardEvent) {
        event.preventDefault();
        const pastedInput: string = event.clipboardData
            .getData('text/plain')
            .replace(/\D/g, '');
        document.execCommand('insertText', false, pastedInput);
    }

    @HostListener('drop', ['$event'])
    public onDrop(event: DragEvent) {
        event.preventDefault();
        const textData = event.dataTransfer.getData('text').replace(/\D/g, '');
        this.inputElement.focus();
        document.execCommand('insertText', false, textData);
    }

    private preventDefault(e: KeyboardEvent) {
        if (
            ( e.shiftKey || ( e.keyCode < KEY_CODE.ZERO || e.keyCode > KEY_CODE.NINE ) ) &&
            ( e.keyCode < KEY_CODE.NUM_PAD_ZERO || e.keyCode > KEY_CODE.NUM_PAD_NINE )
        ) {
            e.preventDefault();
        }
    }
}

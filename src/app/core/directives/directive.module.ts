import { NgModule } from '@angular/core';

import { DigitOnlyDirective } from './digit-only';
import { DisableControlDirective } from './disable-control';

@NgModule({
    declarations: [
        DisableControlDirective,
        DigitOnlyDirective,
    ],
    exports: [
        DisableControlDirective,
        DigitOnlyDirective,
    ],
    providers: [
        DisableControlDirective,
        DigitOnlyDirective,
    ],
})

export class DirectiveModule {}

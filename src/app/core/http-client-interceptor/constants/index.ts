import { HttpHeaders } from '@angular/common/http';

export const HEADERS = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
});

export const STATUS = {
    OK: 200,
    CREATED: 201,
    NOT_FOUND: 404,
    BAD_REQUEST: 403,
    UNAUTHORIZED: 401,
    REFRESH: 403,
};

import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';

import { HttpClientInterceptorResponse, HttpClientInterceptorRequest } from './interceptors';

@NgModule({
    imports: [
        HttpClientModule,
    ],
    providers: [
        HttpClient,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpClientInterceptorRequest,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpClientInterceptorResponse,
            multi: true,
        },
    ],
})

export class HttpClientInterceptorModule {}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { environment } from '../../../../environments/environment';

@Injectable()
export class HttpClientInterceptorRequest implements HttpInterceptor {
    public intercept(req: HttpRequest<Request>, next: HttpHandler): Observable<HttpEvent<Event>> {
        let { url } = req;
        const { params, headers } = req;

        url = environment.origin + url;

        return next.handle(req.clone({ url, params, headers }));
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { tap } from 'rxjs/operators';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

@Injectable()
export class HttpClientInterceptorResponse implements HttpInterceptor {
    public intercept(req: HttpRequest<Request>, next: HttpHandler): Observable<HttpEvent<object>> {
        return next.handle(req)
            .pipe(tap((event: any) => {
                    if (event.body) {
                        event.body = event.body.data || event.body;
                    }
                }));
    }
}

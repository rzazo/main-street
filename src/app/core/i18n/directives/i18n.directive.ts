import { TranslateDirective, TranslateService } from '@ngx-translate/core';
import { Directive, ElementRef, Input, ChangeDetectorRef } from '@angular/core';

@Directive({
    selector: '[i18n-as]',
})
export class I18nDirective extends TranslateDirective {
    @Input()
    public set i18n(key: string) {
        this.translate = key;
    }

    @Input('i18n-params')
    public set i18nParams(params: object) {
        this.translateParams = params;
    }

    constructor(
        private readonly _element: ElementRef,
        private readonly _changeRef: ChangeDetectorRef,
        private readonly _translateService: TranslateService,
    ) {
        super(_translateService, _element, _changeRef);
    }
}

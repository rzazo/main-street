import { I18nCustomLoader } from '../loaders';

export function I18nHttpFactory(http) {
    return new I18nCustomLoader(http);
}

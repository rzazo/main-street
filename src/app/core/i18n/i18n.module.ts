import { HttpClient } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { I18nPipe } from './pipes';
import { I18nModel } from './models';
import { I18nDirective } from './directives';
import { I18nHttpFactory } from './factories';
import { I18nService, I18nRemoteService } from './services';

@NgModule({
    imports: [
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: I18nHttpFactory,
                deps: [
                    HttpClient,
                ],
            },
        }),
    ],
    declarations: [
        I18nPipe,
        I18nDirective,
    ],
    providers: [
        I18nModel,
        I18nService,
        I18nRemoteService,
    ],
    exports: [
        I18nPipe,
        I18nDirective,
    ],
})

export class I18nModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: I18nModule,
        };
    }

    constructor(private readonly service: I18nService) {
        service.addLangs(['en']);
        service.setDefaultLang('en');

        service.use('en');

        service.initData();
    }
}

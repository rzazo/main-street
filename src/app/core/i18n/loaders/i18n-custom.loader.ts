import { Observable, of } from 'rxjs/';
import { HttpClient } from '@angular/common/http';

export class I18nCustomLoader {
    constructor(private readonly http: HttpClient) {}

    public getTranslation(lang: string): Observable<object> {
        return of({});
    }
}

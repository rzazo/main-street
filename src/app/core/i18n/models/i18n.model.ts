import { Injectable } from '@angular/core';

@Injectable()
export class I18nModel {
    private _data: object;

    public get data() {
        return this._data;
    }

    public set data(data: object) {
        this._data = { ...this._data, ...data };
    }
}

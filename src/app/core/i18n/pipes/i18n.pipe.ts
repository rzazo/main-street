import { ChangeDetectorRef, Injectable, Pipe, PipeTransform } from '@angular/core';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';

@Injectable()
@Pipe({ name: 'i18n', pure: false })
export class I18nPipe extends TranslatePipe implements PipeTransform {
    constructor(
        private readonly  _changeRef: ChangeDetectorRef,
        private readonly _translate: TranslateService,
    ) {
        super(_translate, _changeRef);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class I18nRemoteService {
    constructor(private readonly http: HttpClient) {}

    public async getData(type: string, category: string) {
        const params = { category, type };

        return await this.http.get('/api/translates', { params }).toPromise();
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { TranslateService } from '@ngx-translate/core';

import { I18nModel } from '../models/i18n.model';
import { I18nRemoteService } from './i18n-remote.service';

@Injectable()
export class I18nService {
    private currentLang: string;

    constructor(
        private readonly  model: I18nModel,
        private readonly  remoteService: I18nRemoteService,
        private readonly translateService: TranslateService,
    ) {}

    public addLangs(lang: string[]): void {
        this.translateService.addLangs(lang);
    }

    public getParsedResult(translations: string, key: string, params: [{ key: string }]): void {
        this.translateService.getParsedResult(translations, key, params);
    }

    public setDefaultLang(lang: string): void {
        this.translateService.setDefaultLang(lang);
    }

    public use(lang: string): void {
        this.currentLang = lang;
        this.translateService.use(lang);
    }

    public getLangs(): string[] {
        return this.translateService.getLangs();
    }

    public async set(type: string, category: string = 'any') {
        this.model.data = await this.remoteService.getData(type, category);
        this.translateService.setTranslation(this.currentLang, this.model.data);
    }

    public get(key: string, params: { key: string }): Observable<{ key: string }> {
        return this.translateService.get(key, params);
    }

    public initData(): void {
        this.translateService.setTranslation('en', this.model.data);
    }
}

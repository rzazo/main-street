export * from './i18n';
export * from './pipes';
export * from './constants';
export * from './http-client-interceptor';

export { CoreModule } from './core.module';

import { Component, OnInit, OnDestroy } from '@angular/core';

import { ILoader } from '../interfaces';
import { LoaderService } from '../services';
import { Subscription } from 'rxjs/';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit, OnDestroy {

    public loading: ILoader = { show: false };

    private subscription: Subscription;

    constructor(private loaderService: LoaderService) { }

    public ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: ILoader) => {
                setTimeout(() => {
                    this.loading = { ...state };
                }, 0);
            });
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}

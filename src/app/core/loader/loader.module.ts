import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoaderComponent } from './components';
import { LoaderService } from './services';
import { LoaderInterceptorService } from './interceptors';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        LoaderComponent,
    ],
    providers: [
        LoaderService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptorService,
            multi: true,
        },
    ],
    entryComponents: [
        LoaderComponent,
    ],
    exports: [
        LoaderComponent,
    ],
})
export class LoaderModule {}

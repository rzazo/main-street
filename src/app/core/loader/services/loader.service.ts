import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/';

import { ILoader } from '../interfaces';

@Injectable()
export class LoaderService {

    protected loaderSubject = new Subject<ILoader>();

    public loaderState = this.loaderSubject.asObservable();

    public show() {
        this.loaderSubject.next({ show: true });
    }

    public hide() {
        this.loaderSubject.next({ show: false });
    }

}

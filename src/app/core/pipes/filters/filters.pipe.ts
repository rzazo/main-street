import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { MAGIC_NUMBER } from '../../constants/index';

@Pipe({
    name: 'filter',
    pure: true,
})

@Injectable()
export class FiltersPipe implements PipeTransform {
    public transform(items: object[], field: string, value: string): object[] {
        return (items) && items.filter((item) => {
            return [value, ''].indexOf(item[field]) < MAGIC_NUMBER.ZERO && !item[field].includes('/:')
        });
    }
}

import { NgModule } from '@angular/core';

import { JoinPipe } from './join';
import { FiltersPipe } from './filters';
import { CapitalizePipe } from './capitalize';

@NgModule({
    declarations: [
        JoinPipe,
        FiltersPipe,
        CapitalizePipe,
    ],
    exports: [
        JoinPipe,
        FiltersPipe,
        CapitalizePipe,
    ],
    providers: [
        JoinPipe,
        FiltersPipe,
        CapitalizePipe,
    ],
})

export class PipeModule {}

import { Component, Input, OnChanges, OnInit } from '@angular/core';

import { DEFAULT_CONFIG_MAP } from '../constants';
import { icon, latLng, LayerGroup, marker, tileLayer } from 'leaflet';

@Component({
    selector: 'app-map-leaflet',
    templateUrl: './map-leaflet.component.html',
    styleUrls: [ './map-leaflet.component.scss' ]
})

export class MapLeafletComponent implements OnInit, OnChanges {
    @Input() public model;
    @Input() public config = DEFAULT_CONFIG_MAP;

    public configMap;
    public markers: LayerGroup[] = [];

    private latitude = 41.3776123;
    private longitude = 2.1512796;

    public ngOnInit() {
        this.configMap = {
            layers: [
                tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
            ],
            zoom: 5,
            center: latLng(this.latitude, this.longitude),
            controls: {
                baseLayers: {
                    'Open Street Map': tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
                },
                overlays: {
                    'inAtlas': marker([ this.latitude, this.longitude ])
                },
                layers: [
                    marker([ this.latitude, this.longitude ]),
                ]
            }
        };
    }

    public ngOnChanges() {
        if (!!this.model && this.configMap) {
            const layerGroup: LayerGroup = new LayerGroup();

            this.model.forEach((element) => {
                const newMarker = marker(
                    [ element.lat, element.lon ],
                    {
                        icon: icon({
                            iconSize: [ 25, 41 ],
                            iconAnchor: [ 13, 41 ],
                            iconUrl: 'assets/marker-icon.png',
                            shadowUrl: 'assets/marker-shadow.png'
                        })
                    });
                newMarker.addTo(layerGroup);
            });
            this.markers.push(layerGroup);
        }
    }

    public reset() {
        this.markers = [];
    }
}

export const DEFAULT_MARKER = {
    anchor: [0.5, 46],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    opacity: 0.75,
    src: 'https://img.icons8.com/cotton/64/000000/pin3.png'
};

export const DEFAULT_CONFIG_MAP = {
    marker: DEFAULT_MARKER,
};

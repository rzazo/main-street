import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MapLeafletComponent } from './components';

@NgModule({
    declarations: [
        MapLeafletComponent,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        LeafletModule.forRoot()
    ],
    exports: [
        MapLeafletComponent
    ]
})
export class MapLeafletModule {}

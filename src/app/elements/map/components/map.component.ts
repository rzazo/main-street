import { Component, Input, OnChanges, OnInit } from '@angular/core';

import { Map, View, Feature } from 'ol';
import { fromLonLat, transform } from 'ol/proj';
import { IOpenStreetData } from '../interfaces';
import XYZ from 'ol/source/XYZ';
import TileLayer from 'ol/layer/Tile';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Point, LineString } from 'ol/geom';
import { Style, Icon } from 'ol/style';
import { DEFAULT_CONFIG_MAP } from '../constants';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: [ './map.component.scss' ]
})

export class MapComponent implements OnInit, OnChanges {
    @Input() public model: IOpenStreetData;
    @Input() public config = DEFAULT_CONFIG_MAP;

    public map: Map;
    public source: XYZ;
    public layer: TileLayer;
    public view: View;
    public vectorSource: VectorSource;
    public vectorLayer: VectorLayer;

    private latitude = 41.3758075;
    private longitude = 2.1777689;
    public markers = [];

    public ngOnInit() {
        this.config = {...DEFAULT_CONFIG_MAP, ...this.config };
        this.initMap();
    }

    public async ngOnChanges() {
        if (this.model) {
            const layerMarkers = await this.setPoint(this.model);

            this.map.addLayer(layerMarkers);
            console.log('this.vectorLayer', this.vectorLayer.getSource());
        }
    }

    public initMap() {
        this.source = new XYZ({
            url: 'http://tile.osm.org/{z}/{x}/{y}.png',
        });

        this.layer = new TileLayer({
            source: this.source,
        });

        this.view = new View({
            center: fromLonLat([this.longitude, this.latitude]),
            zoom: 3
        });

        this.map = new Map({
            target: 'map',
            layers: [this.layer],
            view: this.view
        });

        this.vectorSource = new VectorSource({});

        this.map.on('click', function (args) {
            const lonlat = transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');

            const lon = lonlat[0];
            const lat = lonlat[1];
            console.log(`lat: ${lat} long: ${lon}`);
        });
    }

    public setCenter(longitude, latitude) {
        const view = this.map.getView();
        view.setCenter(fromLonLat([longitude, latitude]));
        view.setZoom(3);
    }

    public setPoint(data) {
        data.forEach((element, index) => {
            const iconFeature = new Feature({
                geometry: new Point(transform([element.lon, element.lat], 'EPSG:4326',   'EPSG:3857')),
                name: element.display_name
            });

            this.markers[index] = transform([element.lon, element.lat], 'EPSG:4326',   'EPSG:3857');
            this.vectorSource.addFeature(iconFeature);
        });

        const iconStyle = new Style({
            image: new Icon(({
                anchor: this.config.marker.anchor,
                anchorXUnits: this.config.marker.anchorXUnits,
                anchorYUnits: this.config.marker.anchorYUnits,
                opacity: this.config.marker.opacity,
                src: this.config.marker.src
            }))
        });

        this.vectorLayer = new VectorLayer ({
            source: this.vectorSource,
            style: iconStyle
        });

        return this.vectorLayer;
    }

    public reset() {
        this.vectorSource.clear();
    }
}

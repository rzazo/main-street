export const DEFAULT_MARKER = {
    anchor: [0.5, 46],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    opacity: 1,
    src: 'assets/marker-icon.png'
};

export const DEFAULT_CONFIG_MAP = {
    marker: DEFAULT_MARKER,
};

import { Component } from '@angular/core';
import { Routes } from '@angular/router';

import { ROUTES } from '../constants';

@Component({
    selector: 'app-root',
    templateUrl: './content.component.html',
    styleUrls: [ './content.component.scss' ]
})
export class ContentComponent {
    public routes: Routes = ROUTES;
}

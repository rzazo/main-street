import { Routes } from '@angular/router';

export const ROUTES: Routes = [
    {
        path: '',
        loadChildren: '../../pages/home/home.module#HomeModule',
    },
    {
        path: 'home',
        loadChildren: '../../pages/home/home.module#HomeModule',
    },
    {
        path: 'main-street',
        loadChildren: '../../pages/main-street/main-street.module#MainStreetModule',
    },
    {
        path: 'main-street-leaflet',
        loadChildren: '../../pages/main-street-leaflet/main-street-leaflet.module#MainStreetLeafletModule',
    },
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    }
];

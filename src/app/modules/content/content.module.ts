import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { CoreModule } from '../../core';
import { ContentComponent } from './components';
import { HeaderModule, FooterModule } from '../../shared';
import { ContentRoutingModule } from './content-routing.module';

@NgModule({
    declarations: [
        ContentComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        ContentRoutingModule,

        CoreModule,
        HeaderModule,
        FooterModule,
    ],
    entryComponents: [
        ContentComponent
    ],
    bootstrap: [
        ContentComponent
    ]
})

export class ContentModule {}

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { CoreModule } from '../../../core';
import { ContentComponent } from '../components';
import { HeaderModule } from '../../../shared/header';
import { FooterModule } from '../../../shared/footer';

describe('ContentComponent', () => {
    let component: ContentComponent;
    let fixture: ComponentFixture<ContentComponent>;
    let debugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,

                CoreModule,
                HeaderModule,
                FooterModule,
            ],
            declarations: [
                ContentComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContentComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        fixture.detectChanges();
    });

    it('should Content component when render component', () => {
        expect(component).toBeTruthy();
    });

    it('should render Header component when render component', () => {
        const headerComponent = getQueryBy('app-header');
        expect(headerComponent).toBeTruthy();
    });

    it('should render Footer component when render component', () => {
        const footerComponent = getQueryBy('app-footer');
        expect(footerComponent).toBeTruthy();
    });

    it('should render Container to load modules component when render component', () => {
        const routerOutletComponent = getQueryBy('app-header');
        expect(routerOutletComponent).toBeTruthy();
    });

    function getQueryBy(tag: string) {
        return debugElement.query(By.css(tag));
    }
});

import { Routes } from '@angular/router';

import { HomeComponent } from '../components';

export const ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent
    },
];

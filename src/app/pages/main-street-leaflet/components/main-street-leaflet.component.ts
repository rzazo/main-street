import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { faSearchLocation, faArrowRight, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import { FORM_VALIDATIONS } from '../constants';
import { MainStreetLeafletService } from '../services';
import { MapLeafletComponent } from '../../../elements/map-leaflet/components';

@Component({
    selector: 'app-main-street-leaflet',
    templateUrl: './main-street-leaflet.component.html',
    styleUrls: [ './main-street-leaflet.component.scss' ]
})

export class MainStreetLeafletComponent implements OnInit {
    @ViewChild('openMapLeaflet') public openMapLeaflet: MapLeafletComponent;

    public form: FormGroup;
    public title = 'Welcome to Main Street : LeafLet';
    public searchData;
    public icons = {
        search: faSearchLocation,
        arrow: faArrowRight,
        clean: faTrashAlt,
    };

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly service: MainStreetLeafletService,
    ) {}

    public ngOnInit() {
        this.form = this.formBuilder.group(FORM_VALIDATIONS);
    }

    public async searchByCriteria() {
        this.searchData = await this.service.searchStreet({ ...this.form.value, ...{ format: 'json' } });
        this.searchData.forEach((element) => {
            element.lat = +element.lat;
            element.lon = +element.lon;
        });

        this.form.reset();
    }

    public cleanMap() {
        this.openMapLeaflet.reset();
    }
}

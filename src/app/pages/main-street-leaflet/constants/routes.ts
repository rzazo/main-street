import { Routes } from '@angular/router';

import { MainStreetLeafletComponent } from '../components';

export const ROUTES: Routes = [
    {
        path: '',
        component: MainStreetLeafletComponent
    },
];
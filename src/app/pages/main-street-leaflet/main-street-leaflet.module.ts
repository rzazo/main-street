import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MainStreetLeafletComponent } from './components';
import { MapLeafletModule } from '../../elements/map-leaflet/map-leaflet.module';
import { MainStreetLeafletRoutingModule } from './main-street-leaflet-routing.module';
import { MainStreetLeafletService, MainStreetLeafletRemoteService } from './services';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,

        MapLeafletModule,
        MainStreetLeafletRoutingModule,
    ],
    declarations: [
        MainStreetLeafletComponent
    ],
    providers: [
        MainStreetLeafletService,
        MainStreetLeafletRemoteService,
    ],
    exports: [
        MainStreetLeafletComponent
    ]
})

export class MainStreetLeafletModule {}

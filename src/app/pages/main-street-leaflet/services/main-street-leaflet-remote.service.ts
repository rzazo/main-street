import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MainStreetLeafletRemoteService {

  constructor(private readonly httpClient: HttpClient) {}

  public async searchStreet(params) {
      return await this.httpClient.get('https://nominatim.openstreetmap.org/search', { params }).toPromise();
  }
}

import { Injectable } from '@angular/core';
import { MainStreetLeafletRemoteService } from './main-street-leaflet-remote.service';

@Injectable()
export class MainStreetLeafletService {

  constructor(private readonly remote: MainStreetLeafletRemoteService) {}

  public async searchStreet(criteria) {
      return await this.remote.searchStreet(criteria);
    }
}

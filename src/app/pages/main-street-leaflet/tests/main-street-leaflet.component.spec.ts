import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { MainStreetLeafletComponent } from '../components';
import { MapLeafletComponent } from '../../../elements/map-leaflet/components';
import { MapLeafletModule } from '../../../elements/map-leaflet/map-leaflet.module';
import { MainStreetLeafletRemoteService, MainStreetLeafletService } from '../services';

describe('MainStreetLeafletComponent', () => {
    let component: MainStreetLeafletComponent;
    let fixture: ComponentFixture<MainStreetLeafletComponent>;
    let debugElement;
    let services, viewChild, spies;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                ReactiveFormsModule,
                FontAwesomeModule,
                HttpClientTestingModule,

                MapLeafletModule,
            ],
            declarations: [
                MainStreetLeafletComponent
            ],
            providers: [
                MainStreetLeafletService,
                MainStreetLeafletRemoteService,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MainStreetLeafletComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        initServices();

        fixture.detectChanges();
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it(`should have as title 'main-street' when init component`, () => {
        expect(component.title).toEqual('Welcome to Main Street : LeafLet');
    });

    it('should found a message welcome when render title in a h1', () => {
        const title = fixture.debugElement.nativeElement;
        expect(title.querySelector('h1').textContent).toContain('Welcome to Main Street : LeafLet');
    });

    it(`should detect form when render view`, () => {
        const formDiv = getQueryBy('.form-container > .form-group');
        expect(formDiv).toBeTruthy();
    });

    it(`should detect input with icon when render view`, () => {
        const inputSearch = getQueryBy('.form-container > .form-group > .col-6 > .form-control');
        const iconFa = getQueryBy('.form-container > .form-group > .col-6 > .icon-search');

        expect(inputSearch).toBeTruthy();
        expect(iconFa).toBeTruthy();
    });

    it(`should detect button to run search when render view`, () => {
        const buttonSearch = getQueryBy('div.container-fluid > div.form-container > div.form-group > button.btn-secondary');
        expect(buttonSearch).toBeTruthy();
    });

    it(`should call to search for get new model pointers when click on button search`, () => {
        component.form.controls.q.setValue('criteria search');
        const buttonSearch = getQueryBy('div.form-container > div.form-group  > button.btn-secondary');
        buttonSearch.triggerEventHandler('click', null);

        expect(spies.remoteService.searchStreet).toHaveBeenCalled();
    });


    it(`should detect button to clean pointers when render view`, () => {
        const buttonSearch = getQueryBy('div.container-fluid > div.form-container > div.form-group > button.btn-secondary');
        expect(buttonSearch).toBeTruthy();
    });

    it(`should map element is defined when render view`, () => {
        const mapComponent = getQueryBy('div.map-container');
        expect(mapComponent).toBeTruthy();
    });

    it(`should map component functional is defined when render view`, () => {
        const mapChild: MapLeafletComponent = fixture.componentInstance.openMapLeaflet;
        expect(mapChild).toBeDefined();
    });

    it(`should clean pointers when click on button reset`, () => {
        const buttonSearch = getQueryBy('div.form-container > div.form-group  > button.btn-danger');
        buttonSearch.triggerEventHandler('click', null);

        component.cleanMap();
        expect(spies.mapChild.reset).toHaveBeenCalled();
    });

    function initServices() {
        services = {
            remoteService: TestBed.get(MainStreetLeafletRemoteService)
        };
        initSpies();
    }

    function initSpies() {
        spies = {
            remoteService: {
                searchStreet: spyOn(services.remoteService, 'searchStreet'),
            },
            mapChild: {
                reset: spyOn(component.openMapLeaflet, 'reset'),
            }
        };
    }

    function getQueryBy(tag: string) {
        return debugElement.query(By.css(tag));
    }
});

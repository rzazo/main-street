import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FORM_VALIDATIONS } from '../constants';
import { MainStreetService } from '../services';
import { MapComponent } from '../../../elements/map/components';
import { faSearchLocation, faArrowRight, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-main-street',
    templateUrl: './main-street.component.html',
    styleUrls: [ './main-street.component.scss' ]
})

export class MainStreetComponent implements OnInit {
    @ViewChild('openMap') public openMap: MapComponent;

    public form: FormGroup;
    public title = 'Welcome to Main Street: OpenLayers';
    public searchData;
    public icons = {
        search: faSearchLocation,
        arrow: faArrowRight,
        clean: faTrashAlt,
    };

    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly service: MainStreetService,
    ) {}

    public ngOnInit() {
        this.form = this.formBuilder.group(FORM_VALIDATIONS);
    }

    public async searchByCriteria() {
        this.searchData = await this.service.searchStreet({ ...this.form.value, ...{ format: 'json' } });
        this.searchData.forEach((element) => {
            element.lat = +element.lat;
            element.lon = +element.lon;
        });

        this.openMap.setCenter(this.searchData[0].lon, this.searchData[0].lat);
    }

    public cleanMap() {
        this.openMap.reset();
    }
}

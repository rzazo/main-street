import { Routes } from '@angular/router';

import { MainStreetComponent } from '../components';

export const ROUTES: Routes = [
    {
        path: '',
        component: MainStreetComponent
    },
];
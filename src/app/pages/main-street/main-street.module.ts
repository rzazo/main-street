import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MainStreetComponent } from './components';
import { MapModule } from '../../elements/map/map.module';
import { MainStreetRoutingModule } from './main-street-routing.module';
import { MainStreetService, MainStreetRemoteService } from './services';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,

        MapModule,
        MainStreetRoutingModule,
    ],
    declarations: [
        MainStreetComponent
    ],
    providers: [
        MainStreetService,
        MainStreetRemoteService,
    ],
    exports: [
        MainStreetComponent
    ]
})

export class MainStreetModule {}

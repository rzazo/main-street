import { Injectable } from '@angular/core';
import { MainStreetRemoteService } from './main-street-remote.service';

@Injectable()
export class MainStreetService {

  constructor(private readonly remote: MainStreetRemoteService) {}

  public async searchStreet(criteria) {
      return await this.remote.searchStreet(criteria);
    }
}

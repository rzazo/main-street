import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MainStreetComponent } from '../components';
import { MapModule } from '../../../elements/map/map.module';
import { MainStreetRemoteService, MainStreetService } from '../services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MapComponent } from '../../../elements/map/components';

describe('MainStreetComponent', () => {
    let component: MainStreetComponent;
    let fixture: ComponentFixture<MainStreetComponent>;
    let debugElement;
    let services, spies;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                ReactiveFormsModule,
                FontAwesomeModule,
                HttpClientTestingModule,

                MapModule,
            ],
            declarations: [
                MainStreetComponent
            ],
            providers: [
                MainStreetService,
                MainStreetRemoteService,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MainStreetComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        initServices();

        fixture.detectChanges();
    });

    it('should create the Home when render component', () => {
        const homeComponent = fixture.debugElement.componentInstance;
        expect(homeComponent).toBeTruthy();
    });

    it(`should have as title 'Welcome to Main Street: OpenLayers' when render view`, () => {
        const homeComponent = fixture.debugElement.componentInstance;
        expect(homeComponent.title).toEqual('Welcome to Main Street: OpenLayers');
    });

    it('should render title in a h1 tag when render view', () => {
        const homeComponent = fixture.debugElement.nativeElement;
        expect(homeComponent.querySelector('h1').textContent).toContain('Welcome to Main Street: OpenLayers');
    });

    it(`should detect form when render view`, () => {
        const formDiv = getQueryBy('.form-container > .form-group');
        expect(formDiv).toBeTruthy();
    });

    it(`should detect input with icon when render view`, () => {
        const inputSearch = getQueryBy('.form-container > .form-group > .col-6 > .form-control');
        const iconFa = getQueryBy('.form-container > .form-group > .col-6 > .icon-search');

        expect(inputSearch).toBeTruthy();
        expect(iconFa).toBeTruthy();
    });

    it(`should detect button to run search when render view`, () => {
        const buttonSearch = getQueryBy('div.container-fluid > div.form-container > div.form-group > button.btn-secondary');
        expect(buttonSearch).toBeTruthy();
    });

    it(`should call to search for get new model pointers when click on button search`, () => {
        component.form.controls.q.setValue('criteria search');
        const buttonSearch = getQueryBy('div.form-container > div.form-group  > button.btn-secondary');
        buttonSearch.triggerEventHandler('click', null);

        expect(spies.remoteService.searchStreet).toHaveBeenCalled();
    });


    it(`should detect button to clean pointers when render view`, () => {
        const buttonSearch = getQueryBy('div.container-fluid > div.form-container > div.form-group > button.btn-secondary');
        expect(buttonSearch).toBeTruthy();
    });

    it(`should map element is defined when render view`, () => {
        const mapComponent = getQueryBy('div.map-container');
        expect(mapComponent).toBeTruthy();
    });

    it(`should map component functional is defined when render view`, () => {
        const mapChild: MapComponent = fixture.componentInstance.openMap;
        expect(mapChild).toBeDefined();
    });

    it(`should clean pointers when click on button reset`, () => {
        const buttonSearch = getQueryBy('div.form-container > div.form-group  > button.btn-danger');
        buttonSearch.triggerEventHandler('click', null);

        component.cleanMap();
        expect(spies.mapChild.reset).toHaveBeenCalled();
    });

    function initServices() {
        services = {
            remoteService: TestBed.get(MainStreetRemoteService)
        };

        initSpies();
    }

    function initSpies() {
        spies = {
            remoteService: {
                searchStreet: spyOn(services.remoteService, 'searchStreet'),
            },
            mapChild: {
                reset: spyOn(component.openMap, 'reset'),
            }
        };
    }

    function getQueryBy(tag: string) {
        return debugElement.query(By.css(tag));
    }
});

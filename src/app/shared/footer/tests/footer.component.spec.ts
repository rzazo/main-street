import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from '../components/footer.component';
import { By } from '@angular/platform-browser';

describe('FooterComponent', () => {
    let component: FooterComponent;
    let fixture: ComponentFixture<FooterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ FooterComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FooterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should copyright when render', () => {
        const footerImage = fixture.debugElement.query(By.css('.navbar-brand > img'));

        expect(footerImage).toBeTruthy();
    });

    it('should copyright when render', () => {
        const footer = fixture.debugElement.query(By.css('.navbar-brand'));

        expect(footer.nativeElement.textContent.trim()).toBe('Main Street | @2019 Roberto Zazo');
    });
});

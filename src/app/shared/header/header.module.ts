import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PipeModule } from '../../core/pipes';
import { HeaderComponent } from './components/header.component';

@NgModule({
    declarations: [
        HeaderComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,

        PipeModule,
    ],
    exports: [
        HeaderComponent,
    ]
})
export class HeaderModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { PipeModule } from '../../../core';
import { HeaderComponent } from '../components/header.component';

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let debugElement: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeaderComponent
            ],
            imports: [
                PipeModule,
                RouterTestingModule,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show 1 buttons from NAV when receive routes with 1 routes', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
        ];

        const navItems = fixture.debugElement.queryAll(By.css('.nav-link'));
        expect(navItems).toBeTruthy();
    });

    it('should one menu options when receive one path', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
        ];
        fixture.detectChanges();

        const navItems = fixture.debugElement.query(By.css('.nav-link'));
        expect(navItems.nativeElement.textContent.trim()).toBe(component.routes[0].path.toUpperCase());

    });

    it('should show 2 buttons from NAV when receive routes with 2 routes', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
            { path: 'Menu2', loadChildren: 'menu2.module#Menu2Module' },
        ];
        const navItems = fixture.debugElement.queryAll(By.css('.nav-link'));
        expect(navItems).toBeTruthy();
    });

    it('should two menu options when receive two path', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
            { path: 'Menu2', loadChildren: 'menu2.module#Menu2Module' },
        ];
        fixture.detectChanges();

        const navItems = fixture.debugElement.queryAll(By.css('.nav-link'));
        expect(navItems[0].nativeElement.textContent.trim()).toBe(component.routes[0].path.toUpperCase());
        expect(navItems[1].nativeElement.textContent.trim()).toBe(component.routes[1].path.toUpperCase());
    });

    it('should show 3 buttons from NAV when receive routes with 3 routes', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
            { path: 'Menu2', loadChildren: 'menu2.module#Menu2Module' },
            { path: 'Menu3', loadChildren: 'menu3.module#Menu3Module' }
        ];

        const navItems = fixture.debugElement.queryAll(By.css('.nav-link'));
        expect(navItems).toBeTruthy();
    });

    it('should two menu options when receive two path', () => {
        component.routes = [
            { path: 'Menu1', loadChildren: 'menu1.module#Menu1Module' },
            { path: 'Menu2', loadChildren: 'menu2.module#Menu2Module' },
            { path: 'Menu3', loadChildren: 'menu3.module#Menu3Module' }
        ];
        fixture.detectChanges();

        const navItems = fixture.debugElement.queryAll(By.css('.nav-link'));
        expect(navItems[0].nativeElement.textContent.trim()).toBe(component.routes[0].path.toUpperCase());
        expect(navItems[1].nativeElement.textContent.trim()).toBe(component.routes[1].path.toUpperCase());
        expect(navItems[2].nativeElement.textContent.trim()).toBe(component.routes[2].path.toUpperCase());
    });
});
